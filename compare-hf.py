import matplotlib.pyplot as plt
import numpy as np

fig, ax = plt.subplots(ncols=3, nrows=3, figsize=(32,16))

dim = ['x','y','z']

for (i,n) in enumerate([1,2,8]):
    lmp_hf = np.loadtxt(f"ref/hf-lmp-{n}")[:, 3:6]/0.001
    dlp_hf = np.loadtxt(f"actual/HEATFLUX-{n}")[:, 3:6]

    [ax[i,j].plot(lmp_hf[:, j]) for j in range(3)]
    [ax[i,j].plot(dlp_hf[:, j]) for j in range(3)]
    
    [ax[i,j].set_title(f"nprocs: {n}, dimension: {dim[j]}") for j in range(3)]

plt.show()
