DL_POLY=DLPOLY.Z
while getopts "e:" flag
do
 case $flag in 
 	e) DL_POLY="$OPTARG"
 	;;
 esac
done

echo "Using DL_POLY ${DL_POLY}"

#gather lammps reference data
if [ -d ref ]
then
 rm -rf ref
fi
mkdir ref && cp lammpsHeatFlux.in ref/ && cd ref

for n in 1 2 4 6 8
do
	OMP_NUM_THREADS=$n mpirun --use-hwthread-cpus --oversubscribe -np $n lmp -i lammpsHeatFlux.in && tail -n 200 J0Jt.dat > lmp-hfaf 
	grep -E "[0-9]+\s-[0-9]+" log.lammps > lmp-hf-$n
	mv log.lammps log.lammps-$n
	if [ $n -eq 1 ]
	then 
		python3 ../lmp2dlp.py 
	fi
done
cd ..
# now gather dlpoly data
if [ -d actual ]
then
 rm -rf actual
fi

mkdir actual && cp Ar.field CONTROL.new actual/
cp ref/TEST.config actual/ && cd actual
 
for n in 1 2 4 6 8
do
	if [ $n -eq 1 ]
	then
		$DL_POLY -c CONTROL.new
	else
		echo "mpirun --use-hwthread-cpus --oversubscribe -np $n $DL_POLY -c CONTROL.new"
		mpirun --use-hwthread-cpus --oversubscribe -np $n $DL_POLY -c CONTROL.new
	fi
	
	for output in OUTPUT HEATFLUX STATIS
	do
		mv $output $output-$n
	done
	rm REVCON REVIVE
done
cd ..
